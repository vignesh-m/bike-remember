var express = require('express');
var SQLite3 = require('sqlite3').verbose();
var _ = require('lodash');

var db = new SQLite3.Database('bikedb');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var msg = "";
  if (req.query.added)
    msg = "Successfully added.";  
  res.render('index', { title: 'Express' , msg: msg});
});

/* Add bike page. */
router.get('/add', function(req, res, next) {
  res.render('add');
});

/* Add bike submit page */
var bikeInsert = db.prepare('insert into bikes values(?, ?);');
router.post('/add', (req, res) => {
  var data = req.body;
  if (data.bikeno && data.key) {
    // write to db
    bikeInsert.run(data.bikeno, data.key);
  }
  res.redirect('/?added=1');
})

/* Search for partial bikeno*/
var bikeSearch = db.prepare('select distinct bikeno from bikes where bikeno like "$partial%";');
router.get('/search', (req, res) => {
  var qbike = req.query.q;
  if (!qbike) qbike = '';
  console.log(qbike, req.query);
  var query = 'select distinct bikeno, key from bikes where bikeno like "' + qbike + '%";';
  console.log('query');
  db.all(query, function (err, rows) {
    if (err) console.log(err);
    console.log('rows', rows);
    var tr = _.map(rows, el => { return { text: el.bikeno, id: el.bikeno, key: el.key } });
    console.log({ results: tr });
    res.json({ results: tr });
  });
})

module.exports = router;
